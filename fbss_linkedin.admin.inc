<?php

/**
 * @file
 *   Administrative settings for the Facebook-style Statuses linkedin module.
 */

/**
 * The administrative settings form.
 */
function fbss_linkedin_admin() {
  $form = array();
  $form['fbss_linkedin_default'] = array(
    '#type' => 'radios',
    '#title' => t('Default linkedin option'),
    '#default_value' => variable_get('fbss_linkedin_default', 'off'),
    '#required' => TRUE,
    '#options' => array(
      'on' => t('Always enabled by default'),
      'off' => t('Always disabled by default'),
      'on-user' => t('Let the user choose (enabled is default)'),
      'off-user' => t('Let the user choose (disabled is default)'),
      'disallow' => t('Do not allow posting to linkedin at all'),
    ),
    '#weight' => -70,
  );
  return system_settings_form($form);
}

/**
 * Validate function for the Facebook-style Statuses settings form alter.
 */
function fbss_linkedin_admin_validate(&$form, &$form_state) {
  $len = $form_state['values']['facebook_status_length'];
  if (($len > 140 || $len == 0) && variable_get('fbss_linkedin_default', 'off') != 'disallow') {
    $message = t('These settings could allow users to attempt to post messages to linkedin which are too long for linkedin to handle.') .' '.
      t('In this situation, Facebook-style Statuses will attempt to truncate the tweet and include a link to view the full message on your site.') .' '.
      t('Occasionally it is not possible to get a link to the status message, in which case the tweet will simply be shortened to 140 characters by linkedin.');
    if ($len > 140) {
      $message = t('The maximum number of characters allowed in a status is set to a number above 140, and users can post status updates to linkedin.') .' '. $message;
    }
    else {
      $message = t('The maximum number of characters allowed in a status is set to 0 (unlimited), and users can post status updates to linkedin.') .' '. $message;
    }
    drupal_set_message($message, 'warning');
  }
}
